// 数组的定义有两种方法：
// 第一种：在元素类型后面直接接上[],表示由此类型元素组成一个数组
let arrayone : number[] = [1 , 2 , 3]
// 第二种是使用泛型数组Array<元素类型>
let array : Array<number> = [1 , 2 , 3]
