/*
* XMLHttpRequest对象的方法：
*方法                             描述
* new XMLHttpRequest()          创建新的XMLHttpRequest对象
* abort()                       取消当前请求
* getAllResponseHeaders         返回特定的头部信息
*
* open(method , url , async , user , pwd)  规定请求method:请求类型get和post
*                                          url：文件位置
*                                          async:true(异步)false(同步)
*                                          user:可选用户名称
*                                          psw:可选的密码
*
* send                                     将请求发送到服务器，用于get请求
*
* send(string)                             将请求发送到服务器，用于post请求
*
* setRequestHeader                         向要发送的报头添加标签/值对
*
*
*
*
*
* */