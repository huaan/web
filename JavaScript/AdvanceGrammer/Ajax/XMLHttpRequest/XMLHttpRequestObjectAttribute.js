/*
* XMLHttpRequest对象属性
* 属性                                         描述
* onreadystatechange                         定义readyState属性发生变化时被调用的函数
*                                            保存XMLHttpRequest的状态：
*                                            0：请求未初始化
*                                            1：服务器连接已建立
* readyState                                 2：请求已到位
*                                            3：正在处理请求
*                                            4：请求已完成且相应已就绪
*
* responseText                               以字符串返回响应数据
* responseXML                                以xml数据返回响应信息
*                                            返回请求的状态号
*                                            200 ： ok
*                                            403 ： Forbidden
* status                                     404 : not found
*
*
* statusText                                 返回状态文本(比如ok，返回not found)
*
*
*
* */