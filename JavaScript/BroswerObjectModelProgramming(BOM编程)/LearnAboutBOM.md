<style>
h2{
color:red;
text-align:center;
}
p{
color:aqua;
font-size:20px;
text-indent：2em;
}
</style>
<h2>BOM编程</h2>
<p>
BOM编程全称(BroswerObjectProgramming),即浏览器对象编程，旨在在浏览器中，js代码是通过内嵌在浏览器中的Javascript解释器来运行的，在浏览器加载页面的时候，如果页面中有js代码，js解释器会将浏览器的各个部分封装成对应的对象，然后通过访问这些对象的属性和方法来实现特定的功能。
<br>
主要操作对象为:<br>
window：窗体对象，即对浏览器打开的窗口。若HTML文档包含框架(frame),l浏览器会为每个框架创建一个额外的window对象，是BOM的顶层对象。
<br>
document:每个载入浏览器的HTML文档。利用document可以实现对HTML页面中的所有元素进行访问。
<br>
navigator:浏览器对象，包含有关浏览器的信息，例如:浏览器名称、厂商、版本等。
<br>
screen:客户端显示器对象，包含浏览器屏幕的信息，例如:高度、宽度、色彩等。运用这些信息可以优化显示效果，实现用户的显示要求。
<br>
history:历史对象。记录在浏览器窗口中访问过的URL。
<br>
location:浏览器窗口中当前文档的URL地址。
<br>
frame:窗口中所有命名的框架，是window对象的数组。
</p>


