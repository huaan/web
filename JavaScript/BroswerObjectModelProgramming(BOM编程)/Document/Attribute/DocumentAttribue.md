<style>
h2{
text-align:center;
color:red;
}
p{
font-size:20px;
color:aqua;
}
</style>
<h2>Document Attribute</h2>
<p>
常见的Document属性:<br>
all[ ]:提供对文档中所有HTML元素的访问。<br>
anchors[ ]:提供对文档中所有achor对象的引用（超链接对象）。<br>
applets[ ]:提供对文档中所有applet对象的引用。<br>
forms[ ]:提供对文档中所有form对象的引用。<br>
images[ ]:提供文档中所有image对象的引用。<br>
links[ ]：提供对文档中所有area和link对象的引用。<br>
body:提供对body元素的直接访问，对于存在框架集的文档，该属性引用最外层的frameset。<br>
cookie:设置货返回与当前文件夹有关的所有cookie。<br>
domain:返回当前文档的域名。<br>
lastModified:返回文档最后修改时间和日期。<br>
referer:返回载入当前文档的URL。<br>
title:返回当前文档的标题。<br>
URL：返回当前文档的URL。




</p>