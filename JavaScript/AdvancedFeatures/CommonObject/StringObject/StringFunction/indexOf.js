//indexOf:该方法检索一个字符串中是否含有指定内容，如果有该内容，则返回第一次出现的索引；可以指定一个第二参数，指定开始查找的位置，此时返回的是从规定的查找位置开始，第一次查找到的索引
var str = "Hello,World"
console.log(str.indexOf("o"))
console.log(str.indexOf("0" , 5))
