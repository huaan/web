/*substr方法：截取字符串
参数：
第一个参数：截取开始位置索引
第二个参数：截取长度(从开始位置截取)
*/
var str = "Hello,World!"
var result = str.substr(6,6)
console.log(result);